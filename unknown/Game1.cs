﻿// Student: Or Galante 308401710

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;


namespace unknown
{
    // this game is about a hero (the girl character) that gets in the pickle-ricks field, 
    //in order to survive she must fight them with her weapon, or else, they will kill her by stamping on her
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D backGroundTexture;
        private CelAnimationManager celAnimationManager;

        private InputHandler inputHandler;

        private Player player;

        private System.Collections.Generic.List<Enemy> enemies;


        private SpriteFont goalFont;
        private SpriteFont x_20;

        public Game1()
        {
            // Graphics first settings screen resolution.
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1000;
            graphics.PreferredBackBufferHeight = 600;


            Content.RootDirectory = "Content";

            inputHandler = new InputHandler(this);
            Components.Add(inputHandler);

            celAnimationManager = new CelAnimationManager(this, "Textures\\");
            Components.Add(celAnimationManager);

            player = new Player(this);
            Components.Add(player);

            //creating and positioning all the "Pickle ricks" on screen
            enemies = new System.Collections.Generic.List<Enemy>();
            for (int i = 0; i < 5; i++)
            {
                enemies.Add(new Enemy(this, player));
                enemies[i].Position = new Vector2(400 / (i + 1), 50 * (i + 1));
                Components.Add(enemies[i]);
            }
        }


        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //Load the texture that holds the actual sprites.
            backGroundTexture = Content.Load<Texture2D>(@"Textures\97189");

            // Load the font for the goals
            goalFont = Content.Load<SpriteFont>(@"Fonts\Arial");
            x_20 = Content.Load<SpriteFont>(@"Fonts\ComicSansMS_20");

            player.Load(spriteBatch);
            for (int i = 0; i < 5; i++)
                enemies[i].Load(spriteBatch);

        }


        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // in this loop  5 picklericks are being checked for their lives.
            // if the hero kills one of them then he gets removed from components and the hero gets 1000 extra points.
            for (int i = 0; i < 5; i++)
            {
                if (Components.Contains(enemies[i]))
                {
                    // Run in a random direction
                    Random rand = new Random(); //random number
                    int num = (i * 3) / 5;
                    if (enemies[i].pCounter == 0)
                    {
                        Components.Remove(enemies[i]);
                        player.energy += 1000; // the score 
                    }
                    // in order to make sure not all of them are going at the same direction on screen
                    if (gameTime.TotalGameTime.Seconds % 101 == 0)
                    {
                        enemies[i].setDirection(num);
                    }
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();


            // Draw background
            spriteBatch.Draw(backGroundTexture, new Vector2(0, 0), new Rectangle(0, 0, 1000, 600), Color.White);

            // Draw life status
            spriteBatch.DrawString(goalFont, player.energy.ToString() + " Energy", new Vector2(50, 10), Color.WhiteSmoke);

            bool endGame = true;
            // to drow above each "picklerick" his life points out of his beginnig 200 points
            for (int i = 0; i < 5; i++)
            {
                if (Components.Contains(enemies[i]))
                {
                    endGame = false;
                    spriteBatch.DrawString(x_20, enemies[i].pCounter.ToString() + "/200", new Vector2(enemies[i].Position.X, enemies[i].Position.Y - 20), Color.DarkRed);
                }
            }

            //a massage for the winner
            if (endGame)
                spriteBatch.DrawString(x_20, "You Won! with " + player.energy.ToString() + " energy!", new Vector2(500, 500), Color.White);
            //a massage for the loser
            if (player.energy == 0)
                spriteBatch.DrawString(x_20, "You Lost! ", new Vector2(500, 500), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);

        }
    }
}
