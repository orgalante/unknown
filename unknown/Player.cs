using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;

namespace unknown
{
    // this class is the hero of the game (the player)
    // it has 2 goals:
    //1. keep away from the pickle-ricks in order to keep its life point.
    //2. attacking the pickle-ricks in order to gain extra life-point and finaly to win the game.
    public class Player : Microsoft.Xna.Framework.DrawableGameComponent
    {
        ICelAnimationManager celAnimationManager;
        IInputHandler inputHandler;

        SpriteBatch spriteBatch;
        public int energy = 2000; // hero starts with 2000 life points
        private Vector2 position = new Vector2(100, 300);
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        enum Direction { Left, Right, Up, Down };
        private bool poke = false;

        private Direction direction = Direction.Right;
        private float moveRate = 1.0f;

        public Player(Game game): base(game)
        {
            celAnimationManager = (ICelAnimationManager)game.Services.GetService(typeof(ICelAnimationManager));
            inputHandler = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        protected override void LoadContent()
        {
            CelCount celCount = new CelCount(13, 21);

            // Walk animation up
            celAnimationManager.AddAnimation("ladyWalkUp", "ladySprite", celCount, 10, 9, 9, 1);
            celAnimationManager.ToggleAnimation("ladyWalkUp");
            // Walk animation down
            celAnimationManager.AddAnimation("ladyWalkDown", "ladySprite", celCount, 10, 9, 11, 1);
            celAnimationManager.ToggleAnimation("ladyWalkDown");
            // Walk animation left+right directions
            celAnimationManager.AddAnimation("ladyWalk", "ladySprite", celCount, 10, 9, 12, 1);
            celAnimationManager.ToggleAnimation("ladyWalk");

            // poke animation up
            celAnimationManager.AddAnimation("ladyPokeUp", "ladySprite", celCount, 10, 8, 5, 1);
            celAnimationManager.ToggleAnimation("ladyPokeUp");
            // poke animation down
            celAnimationManager.AddAnimation("ladyPokeDown", "ladySprite", celCount, 10, 8, 7, 1);
            celAnimationManager.ToggleAnimation("ladyPokeDown");
            // poke animation left+right directions
            celAnimationManager.AddAnimation("ladyPoke", "ladySprite", celCount, 10, 8, 8, 1);
            celAnimationManager.ToggleAnimation("ladyPoke");

        }

        public override void Update(GameTime gameTime)
        {
            // Poke
            if (inputHandler.KeyboardHandler.IsKeyDown(Keys.P))
            {
                poke = true;
                if (direction == Direction.Up)
                    celAnimationManager.ResumeAnimation("ladyPokeUp");
                else if (direction == Direction.Down)
                    celAnimationManager.ResumeAnimation("ladyPokeDown");
                else
                    celAnimationManager.ResumeAnimation("ladyPoke");
            }
            else
            {
                celAnimationManager.PauseAnimation("ladyPokeDown");
                celAnimationManager.PauseAnimation("ladyPokeUp");
                celAnimationManager.PauseAnimation("ladyPoke");
                poke = false;

                // Up
                if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Up))
                {
                    celAnimationManager.ResumeAnimation("ladyWalkUp");
                    direction = Direction.Up;
                    position.Y -= moveRate;
                }
                else
                    celAnimationManager.PauseAnimation("ladyWalkUp");
                // Down
                if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Down))
                {
                    celAnimationManager.ResumeAnimation("ladyWalkDown");
                    direction = Direction.Down;
                    position.Y += moveRate;
                }
                else
                    celAnimationManager.PauseAnimation("ladyWalkDown");

                // Right & Left
                if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Right))
                {
                    celAnimationManager.ResumeAnimation("ladyWalk");
                    direction = Direction.Right;
                    position.X += moveRate;
                }
                else if (inputHandler.KeyboardHandler.IsKeyDown(Keys.Left))
                {
                    celAnimationManager.ResumeAnimation("ladyWalk");
                    direction = Direction.Left;
                    position.X -= moveRate;
                }
                else
                    celAnimationManager.PauseAnimation("ladyWalk");
            }

            int celWidth = celAnimationManager.GetAnimationFrameWidth("ladyWalk");
            int celHeight = celAnimationManager.GetAnimationFrameHeight("ladyWalk");
            // Making boundries for character moves on screen:
            if (position.X > (Game.GraphicsDevice.Viewport.Width - celWidth))
                position.X = (Game.GraphicsDevice.Viewport.Width - celWidth);
            if (position.X < 0)
                position.X = 0;
            if (position.Y > (Game.GraphicsDevice.Viewport.Height - celHeight))
                position.Y = (Game.GraphicsDevice.Viewport.Height - celHeight);
            if (position.Y < 0)
                position.Y = 0;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            if (poke)
            {
                if (direction == Direction.Up)
                    celAnimationManager.Draw(gameTime, "ladyPokeUp", spriteBatch, position, SpriteEffects.None);
                else if (direction == Direction.Down)
                    celAnimationManager.Draw(gameTime, "ladyPokeDown", spriteBatch, position, SpriteEffects.None);
                else
                    celAnimationManager.Draw(gameTime, "ladyPoke", spriteBatch, position, direction == Direction.Right ? SpriteEffects.None : SpriteEffects.FlipHorizontally);
            }
            else if (direction == Direction.Up || direction == Direction.Down)
                celAnimationManager.Draw(gameTime, direction == Direction.Up ? "ladyWalkUp" : "ladyWalkDown", spriteBatch, position, SpriteEffects.None);
            else
                celAnimationManager.Draw(gameTime, "ladyWalk", spriteBatch, position, direction == Direction.Right ? SpriteEffects.None : SpriteEffects.FlipHorizontally);
            spriteBatch.End();
        }
    }
}
