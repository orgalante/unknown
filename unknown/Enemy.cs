// Student: Or Galante 308401710
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;


namespace unknown
{
    // this is the pickle-rick class (the enemy)
    // his behavior is walking from side to side on screen.
    // if he move over the hero without getting hit by her, he will take away some of her life points
    public class Enemy : Microsoft.Xna.Framework.DrawableGameComponent
    {
        ICelAnimationManager celAnimationManager;
        IInputHandler inputHandler;

        SpriteBatch spriteBatch;

        bool playerNear = false;

        private Vector2 position = new Vector2(50, 200);
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        public enum Direction { Left, Right, Up, Down };
        public Direction direction = Direction.Left;

        public int pCounter = 200; // start life point for enemy

        private readonly float moveRate = 1.5f;

        // Acts according to player position and behaviour
        private Player player;

        public Enemy(Game game, Player player) : base(game)
        {
            celAnimationManager = (ICelAnimationManager)game.Services.GetService(typeof(ICelAnimationManager));
            inputHandler = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
            this.player = player;
        }
        // helps making sure not all enemies go in the same direction.
        public void setDirection(int num)
        {
            switch (num)
            {
                case 0:
                    direction = Direction.Down;
                    break;
                case 1:
                    direction = Direction.Left;
                    break;
                case 2:
                    direction = Direction.Right;
                    break;
                case 3:
                    direction = Direction.Up;
                    break;
            }
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }


        protected override void LoadContent()
        {
            CelCount celCount = new CelCount(4, 3);

            // Walk animation up
            celAnimationManager.AddAnimation("PickleRickUp", "PickleRickSprite", celCount, 10, 4, 3, 1);
            celAnimationManager.ToggleAnimation("PickleRickUp");
            // Walk animation down
            celAnimationManager.AddAnimation("PickleRickDown", "PickleRickSprite", celCount, 10, 4, 1, 1);
            celAnimationManager.ToggleAnimation("PickleRickDown");
            // Walk animation left+right directions
            celAnimationManager.AddAnimation("PickleRickWalk", "PickleRickSprite", celCount, 7, 4, 2, 1);
            celAnimationManager.ToggleAnimation("PickleRickWalk");

            CelCount celCountBlood = new CelCount(19, 8);
            // Blood animation
            celAnimationManager.AddAnimation("Blood", "BloodSprite", celCountBlood, 5, 19, 1, 1);
            celAnimationManager.ToggleAnimation("Blood");

        }

        public override void Update(GameTime gameTime)
        {
            if (!inputHandler.KeyboardHandler.IsKeyDown(Keys.P))
            {
                

                if (position.X < 0) // need to turn to other side
                {
                    direction = Direction.Right;
                    position.X = 0;
                }
                else if (position.X >= Game.GraphicsDevice.Viewport.Width - 50)
                {
                    direction = Direction.Left;
                    position.X = Game.GraphicsDevice.Viewport.Width - 50;
                }
                if (position.Y > Game.GraphicsDevice.Viewport.Height - 50)
                {
                    direction = Direction.Up;
                    position.Y = Game.GraphicsDevice.Viewport.Height - 50;
                }
                else if (position.Y < 0)
                {
                    direction = Direction.Down;
                    position.Y = 0;
                }

                // pause them all
                celAnimationManager.PauseAnimation("PickleRickDown");
                celAnimationManager.PauseAnimation("PickleRickUp");
                celAnimationManager.PauseAnimation("PickleRickWalk");

                // controls the enemy movement on screen drawings
                if (direction == Direction.Down)
                {
                    position.Y += moveRate;
                    celAnimationManager.ResumeAnimation("PickleRickDown");
                }
                else if (direction == Direction.Up)
                {
                    position.Y -= moveRate;
                    celAnimationManager.ResumeAnimation("PickleRickUp");
                }
                else
                {
                    celAnimationManager.ResumeAnimation("PickleRickWalk");
                    if (direction == Direction.Right)
                        position.X += moveRate;
                    else
                        position.X -= moveRate;
                }
            }
            //in case enemy being hit again
            else
            {
                celAnimationManager.ResumeAnimation("Blood");
            }

            int distY = 100;
            int distX = 50;
            // Check for collision 
            if (((player.Position.X > Position.X && player.Position.X <Position.X + distX) ||
                 (player.Position.X + distX > Position.X &&
                 player.Position.X + distX < Position.X + distX)) &&
                 ((player.Position.Y >Position.Y &&
                 player.Position.Y < Position.Y + distY) ||
                 (player.Position.Y + distY > Position.Y &&
                 player.Position.Y + distY < Position.Y + distY))&&player.energy>0)
            {
                player.energy--;
                playerNear = true;
            }
            else
                playerNear = false;
                

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            if (!inputHandler.KeyboardHandler.IsKeyDown(Keys.P))
            {
                
                if (direction == Direction.Up)
                    celAnimationManager.Draw(gameTime, "PickleRickUp", spriteBatch, position, SpriteEffects.None);
                else if (direction == Direction.Down)
                    celAnimationManager.Draw(gameTime, "PickleRickDown", spriteBatch, position, SpriteEffects.None);
                else
                    celAnimationManager.Draw(gameTime, "PickleRickWalk", spriteBatch, position, direction == Direction.Left ? SpriteEffects.None : SpriteEffects.FlipHorizontally);
            }
            else if (playerNear && pCounter > 0)
            {
                pCounter--;
                celAnimationManager.Draw(gameTime, "PickleRickDown", spriteBatch, position, SpriteEffects.None);
                celAnimationManager.Draw(gameTime, "Blood", spriteBatch, new Vector2(position.X - 50, position.Y + 10), SpriteEffects.None);

            }
            else
            {
                celAnimationManager.Draw(gameTime, "PickleRickDown", spriteBatch, position, SpriteEffects.None);
            }

            spriteBatch.End();
        }
    }
}
